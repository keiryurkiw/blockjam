/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * BlockJam is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BlockJam is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BlockJam.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * NOTE: This is a terrible way of allocating memory, but I'm going
 * with it because I'm short on time.
 */

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <volk.h>

#include "../utils.h"
#include "alloc.h"
#include "device.h"
#include "utils.h"

static uint32_t
find_mem_props(VulkDevice *device, uint32_t type_bits,
		VkMemoryPropertyFlags prop_flags)
{
	uint32_t count = device->mem_properties.memoryTypeCount;
	for (uint32_t i = 0; i < count; i++) {
		bool is_required_type = type_bits & ((uint32_t)1 << i);

		VkMemoryPropertyFlags props =
			device->mem_properties.memoryTypes[i].propertyFlags;
		bool has_required_props = (props & prop_flags) == prop_flags;

		if (is_required_type && has_required_props)
			return i;
	}

	return UINT32_MAX;
}

bool
vulk_alloc(VulkDevice *device, VulkAllocCreateInfo *create_info,
		VulkAlloc *alloc)
{
	alloc->type = create_info->type;

	VkMemoryRequirements requirements;
	VkMemoryPropertyFlags flags;

	if (create_info->type == VULK_ALLOC_TYPE_IMAGE) {
		device->fn.vkCreateImage(device->handle, create_info->info.image,
				NULL, &alloc->handle.image);
		device->fn.vkGetImageMemoryRequirements(device->handle,
				alloc->handle.image, &requirements);
		flags =
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
			VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
	} else if (create_info->type == VULK_ALLOC_TYPE_BUFFER) {
		device->fn.vkCreateBuffer(device->handle, create_info->info.buffer,
				NULL, &alloc->handle.buffer);
		device->fn.vkGetBufferMemoryRequirements(device->handle,
				alloc->handle.buffer, &requirements);
		flags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
	} else {
		assert(false && "Bad memory type given to vulk_alloc()");
	}

	alloc->size = requirements.size;

	uint32_t type = find_mem_props(device, requirements.memoryTypeBits, flags);
	if (type == UINT32_MAX) {
		if (create_info->type == VULK_ALLOC_TYPE_IMAGE) {
			return false;
		} else if (create_info->type == VULK_ALLOC_TYPE_BUFFER) {
			flags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;
			type = find_mem_props(device, requirements.memoryTypeBits, flags);
			if (type == UINT32_MAX)
				return false;
		}
	}

	VkMemoryAllocateInfo alloc_info = {
		.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
		.allocationSize = requirements.size,
		.memoryTypeIndex = type,
	};

	VkResult ret = device->fn.vkAllocateMemory(device->handle,
			&alloc_info, NULL, &alloc->memory);
	if (VULK_IS_ERR(ret)) {
		perr("Vulkan failed to allocate %zu bytes", requirements.size);
		return false;
	}

	if (create_info->type == VULK_ALLOC_TYPE_IMAGE) {
		device->fn.vkBindImageMemory(device->handle,
				alloc->handle.image, alloc->memory, 0);
	} else if (create_info->type == VULK_ALLOC_TYPE_IMAGE) {
		device->fn.vkBindBufferMemory(device->handle,
				alloc->handle.buffer, alloc->memory, 0);
	}

	return true;
}

void
vulk_free(VulkDevice *device, VulkAlloc *alloc)
{
	if (alloc->type == VULK_ALLOC_TYPE_IMAGE)
		device->fn.vkDestroyImage(device->handle, alloc->handle.image, NULL);
	else if (alloc->type == VULK_ALLOC_TYPE_BUFFER)
		device->fn.vkDestroyBuffer(device->handle, alloc->handle.buffer, NULL);
	device->fn.vkFreeMemory(device->handle, alloc->memory, NULL);
}
