/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * BlockJam is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BlockJam is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BlockJam.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VULKAN_ALLOC_H
#define VULKAN_ALLOC_H

#include <stdbool.h>
#include <volk.h>

#include "device.h"

typedef enum {
	VULK_ALLOC_TYPE_IMAGE,
	VULK_ALLOC_TYPE_BUFFER,
} VulkAllocType;

typedef struct {
	VulkAllocType type;
	union {
		VkImageCreateInfo *image;
		VkBufferCreateInfo *buffer;
	} info;
} VulkAllocCreateInfo;

typedef struct {
	VulkAllocType type;
	union {
		VkImage image;
		VkBuffer buffer;
	} handle;

	VkDeviceMemory memory;
	VkDeviceSize size;
} VulkAlloc;

bool vulk_alloc(VulkDevice *device, VulkAllocCreateInfo *create_info,
		VulkAlloc *alloc);
void vulk_free(VulkDevice *device, VulkAlloc *alloc);

#endif /* VULKAN_ALLOC_H */
