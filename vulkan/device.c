/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * BlockJam is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BlockJam is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BlockJam.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <limits.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <volk.h>

#include "../platform/utils.h"
#include "../platform/window.h"
#include "../utils.h"
#include "device.h"
#include "utils.h"

static const bool required_fam_indices[4] = {
	true, /* Graphics */
	false, /* Compute */
	false, /* Transfer */
	true, /* Present */
};

static const char *const required_extensions[] = {
	VK_KHR_SWAPCHAIN_EXTENSION_NAME,
};
static const size_t required_ext_count =
	sizeof(required_extensions) / sizeof(required_extensions[0]);

static void
find_fam_indices(VkPhysicalDevice device, uint32_t indices[4])
{
	uint32_t count;
	vkGetPhysicalDeviceQueueFamilyProperties(device, &count, NULL);
	VkQueueFamilyProperties *props = mem_alloc(count * sizeof(*props));
	vkGetPhysicalDeviceQueueFamilyProperties(device, &count, props);

	memset(indices, 0xFF, sizeof(uint32_t) * 4);

	int min_xfr_count = INT_MAX; /* Try to get dedicated transfer queue */
	for (size_t i = 0; i < count; i++) {
		int xfr_count = 0;

		if (props[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
			GET_GFX_FAM_INDEX(indices) = i;
			++xfr_count;
		}
		if (props[i].queueFlags & VK_QUEUE_COMPUTE_BIT) {
			GET_CMP_FAM_INDEX(indices) = i;
			++xfr_count;
		}
		if (win_is_present_supported(device, i)) {
			GET_PRS_FAM_INDEX(indices) = i;
			++xfr_count;
		}
		if (props[i].queueFlags & VK_QUEUE_TRANSFER_BIT) {
			if (xfr_count <= min_xfr_count) {
				GET_XFR_FAM_INDEX(indices) = i;
				min_xfr_count = xfr_count;
			}
		}
	}

	mem_free(props);
}

static bool
exts_are_supported(VkPhysicalDevice device, const char *const *exts,
		uint32_t ext_count)
{
	if (ext_count == 0 || exts == NULL || exts[0] == NULL)
		return true;

	uint32_t count;
	VkResult res = vkEnumerateDeviceExtensionProperties(
			device, NULL, &count, NULL);
	if (VULK_IS_ERR(res)) {
		perr("Failed to enumerate device extensions");
		return false;
	}
	if (count == 0)
		return false;

	VkExtensionProperties *props = mem_alloc(count * sizeof(*props));
	vkEnumerateDeviceExtensionProperties(device, NULL, &count, props);
	for (size_t i = 0; i < ext_count; i++) {
		bool found = false;
		for (size_t j = 0; j < count; j++) {
			if (strcmp(exts[i], props[j].extensionName) == 0) {
				found = true;
				break;
			}
		}

		if (!found)
			return false;
	}

	mem_free(props);

	return true;
}

static void
get_surface_props(VkPhysicalDevice physical, VkSurfaceKHR surface,
		VulkDevice *device)
{
	VulkSurfProps *sprops = &device->surface_props;
	memset(sprops, 0x00, sizeof(*sprops));

	vkGetPhysicalDeviceSurfaceFormatsKHR(physical, surface,
			&sprops->format_count, NULL);
	if (sprops->format_count > 0) {
		sprops->formats = mem_alloc(
				sprops->format_count * sizeof(*sprops->formats));
		vkGetPhysicalDeviceSurfaceFormatsKHR(physical, surface,
				&sprops->format_count, sprops->formats);
	}

	vkGetPhysicalDeviceSurfacePresentModesKHR(physical, surface,
			&sprops->mode_count, NULL);
	if (sprops->mode_count > 0) {
		sprops->modes = mem_alloc(
				sprops->mode_count * sizeof(*sprops->modes));
		vkGetPhysicalDeviceSurfacePresentModesKHR(physical, surface,
				&sprops->mode_count, sprops->modes);
	}

	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
			physical, surface, &sprops->capabilities);
}

static bool
device_meets_requirements(VkPhysicalDevice physical, VkSurfaceKHR surf,
		VulkDevice *device)
{
	uint32_t *indices = device->queue_family_indices;
	find_fam_indices(physical, indices);
	for (size_t i = 0; i < 4; i++)
		if (required_fam_indices[i] && (indices[i] == UINT32_MAX))
			return false;

	if (!exts_are_supported(physical, required_extensions, required_ext_count))
		return false;

	get_surface_props(physical, surf, device);
	if (device->surface_props.format_count == 0 ||
			device->surface_props.mode_count == 0) {
		mem_free(device->surface_props.formats);
		mem_free(device->surface_props.modes);
		return false;
	}

	return true;
}

static bool
select_physical_device(VkInstance inst, VkSurfaceKHR surf, VulkDevice *device)
{
	uint32_t count;
	VkResult res = vkEnumeratePhysicalDevices(inst, &count, NULL);
	if (VULK_IS_ERR(res)) {
		perr("Failed to enumerate physical devices (%s)", vulk_strerr(res));
		return false;
	}
	if (count == 0) {
		perr("No physical devices were found");
		return false;
	}

	VkPhysicalDevice *devices = mem_alloc(count * sizeof(*devices));
	vkEnumeratePhysicalDevices(inst, &count, devices);

	bool found;
	for (size_t i = 0; i < count; i++) {
		found = device_meets_requirements(devices[i], surf, device);
		if (found) {
			device->physical = devices[i];
			break;
		}
	}

	mem_free(devices);

	return found;
}

static bool
create_device(VulkDevice *device)
{
	VkPhysicalDeviceSynchronization2Features sync_2_features = {
		.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SYNCHRONIZATION_2_FEATURES,
		.synchronization2 = VK_TRUE,
	};

	VkPhysicalDeviceDynamicRenderingFeatures dyn_rend_features = {
		.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DYNAMIC_RENDERING_FEATURES,
		.pNext = &sync_2_features,
		.dynamicRendering = VK_TRUE,
	};

	uint32_t queue_indices[2] = {
		GET_GFX_FAM_INDEX(device->queue_family_indices),
		GET_PRS_FAM_INDEX(device->queue_family_indices),
	};
	unsigned int index_count = (queue_indices[0] == queue_indices[1]) ? 1 : 2;

	VkDeviceQueueCreateInfo queue_infos[2];
	float queue_priority = 1.0f;
	for (size_t i = 0; i < index_count; i++) {
		queue_infos[i] = (VkDeviceQueueCreateInfo){
			.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
			.queueFamilyIndex = queue_indices[i],
			.queueCount = 1,
			.pQueuePriorities = &queue_priority,
		};
	}

	VkDeviceCreateInfo device_info = {
		.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
		.pNext = &dyn_rend_features,
		.pQueueCreateInfos = queue_infos,
		.queueCreateInfoCount = index_count,
		.ppEnabledExtensionNames = required_extensions,
		.enabledExtensionCount = required_ext_count,
	};

	VkResult ret = vkCreateDevice(device->physical,
			&device_info, NULL, &device->handle);
	if (ret != VK_SUCCESS) {
		perr("Failed to create Vulkan device (%s)", vulk_strerr(ret));
		return false;
	}

	volkLoadDeviceTable(&device->fn, device->handle);

	device->fn.vkGetDeviceQueue(device->handle,
			GET_GFX_FAM_INDEX(device->queue_family_indices),
			0, &device->gfx_queue);
	device->fn.vkGetDeviceQueue(device->handle,
			GET_PRS_FAM_INDEX(device->queue_family_indices),
			0, &device->prs_queue);

	return true;
}

VulkDevice *
vulk_dev_create(VkInstance inst, Window win)
{
	/* We're heap allocating because devices are massive */
	VulkDevice *device = mem_alloc(sizeof(*device));

	if (!select_physical_device(inst, win_get_surface(win), device))
		goto fail;
	vkGetPhysicalDeviceProperties(device->physical, &device->properties);
	vkGetPhysicalDeviceMemoryProperties(device->physical,
			&device->mem_properties);

	if (!create_device(device))
		goto fail;

	return device;

fail:
	mem_free(device);
	perr("Failed to create Vulkan device");
	return NULL;
}

void
vulk_dev_destroy(VulkDevice *device)
{
	mem_free(device->surface_props.formats);
	mem_free(device->surface_props.modes);
	device->fn.vkDestroyDevice(device->handle, NULL);
	mem_free(device);
}

const uint32_t *
vulk_dev_get_family_indices(VulkDevice *device)
{
	return device->queue_family_indices;
}
