/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * BlockJam is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BlockJam is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BlockJam.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VULKAN_SWAPCHAIN_H
#define VULKAN_SWAPCHAIN_H

#include <stdint.h>
#include <volk.h>

#include "alloc.h"
#include "device.h"

typedef struct {
	VkSurfaceFormatKHR format;
	VkPresentModeKHR mode;
	VkExtent2D extent;

	VkSwapchainKHR handle;
	VkImage *images;
	VkImageView *image_views;
	uint32_t image_count;

	VulkAlloc depth_image;
	VkImageView depth_view;
} VulkSwapchain;

VkSurfaceFormatKHR vulk_swp_get_format(VulkSurfProps *props);

VulkSwapchain *vulk_swp_create(Window win, VulkDevice *device);
void vulk_swp_destroy(VulkDevice *device, VulkSwapchain *swapchain);
VulkSwapchain *vulk_swp_recreate(Window win, VulkDevice *device,
		VulkSwapchain *swapchain);

#endif /* VULKAN_SWAPCHAIN_H */
