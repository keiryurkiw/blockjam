/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * This file is part of BlockJam.
 *
 * BlockJam is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BlockJam is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BlockJam.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VULKAN_UTILS_H
#define VULKAN_UTILS_H

#include <volk.h>

#define VULK_IS_ERR(_res) ((_res) < VK_SUCCESS)

/* strerror() for VkResult return codes */
char *vulk_strerr(VkResult res);

#ifndef NDEBUG
/* NOTE: This should not be called direcly. Use the macro instead. */
void _vulk_assert(VkResult res, char *cond_str, char *file, int line);
#define VULK_ASSERT(_cond) _vulk_assert(_cond, #_cond, __FILE__, __LINE__)
#else
#define VULK_ASSERT(_cond) ((void)0)
#endif /* NDEBUG */

#endif /* VK_UTILS_H */
