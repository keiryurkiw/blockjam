/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * BlockJam is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BlockJam is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BlockJam.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <volk.h>

#include "../platform/utils.h"
#include "../utils.h"
#include "alloc.h"
#include "device.h"
#include "swapchain.h"
#include "utils.h"

static const VkPresentModeKHR preferred_mode = VK_PRESENT_MODE_MAILBOX_KHR;
static const VkSurfaceFormatKHR preferred_format = {
	.format = VK_FORMAT_B8G8R8A8_UNORM,
	.colorSpace = VK_COLORSPACE_SRGB_NONLINEAR_KHR,
};

VkSurfaceFormatKHR
vulk_swp_get_format(VulkSurfProps *props)
{
	for (size_t i = 0; i < props->format_count; i++) {
		VkSurfaceFormatKHR *fmt = &props->formats[i];
		bool same_format = fmt->format == preferred_format.format;
		bool same_colourspace = fmt->colorSpace == preferred_format.colorSpace;
		if (same_format && same_colourspace)
			return props->formats[i];
	}

	return props->formats[0];
}

static VkPresentModeKHR
choose_present_mode(VulkSurfProps *props)
{
	for (size_t i = 0; i < props->mode_count; i++)
		if (props->modes[i] == preferred_mode)
			return props->modes[i];

	return VK_PRESENT_MODE_FIFO_KHR;
}

static VkExtent2D
adjust_extent(Window win, VulkSurfProps *props)
{
	VkSurfaceCapabilitiesKHR *caps = &props->capabilities;
	if (caps->currentExtent.width != UINT32_MAX)
		return caps->currentExtent;

	uint32_t w, h;
	win_get_fb_size(win, &w, &h);

	w = CLAMP(w, caps->minImageExtent.width, caps->maxImageExtent.width);
	h = CLAMP(h, caps->minImageExtent.height, caps->maxImageExtent.height);

	return (VkExtent2D){ .width = w, .height = h };
}

static void
create_image_views(VulkDevice *device, VulkSwapchain *swapchain)
{
	for (size_t i = 0; i < swapchain->image_count; i++) {
		VkImageViewCreateInfo view_info = {
			.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
			.image = swapchain->images[i],
			.viewType = VK_IMAGE_VIEW_TYPE_2D,
			.format = swapchain->format.format,
			.components = (VkComponentMapping){
				.r = VK_COMPONENT_SWIZZLE_IDENTITY,
				.g = VK_COMPONENT_SWIZZLE_IDENTITY,
				.b = VK_COMPONENT_SWIZZLE_IDENTITY,
				.a = VK_COMPONENT_SWIZZLE_IDENTITY,
			},
			.subresourceRange = (VkImageSubresourceRange){
				.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
				.baseMipLevel = 0,
				.levelCount = 1,
				.baseArrayLayer = 0,
				.layerCount = 1,
			},
		};

		VkResult ret = device->fn.vkCreateImageView(device->handle, &view_info,
				NULL, &swapchain->image_views[i]);
		if (VULK_IS_ERR(ret)) {
			perr("Failed to create swapchain image view #%zu (%s)",
					i, vulk_strerr(ret));
		}
	}
}

static bool
create_depth_image(VulkDevice *device, VulkSwapchain *swapchain)
{
	VkImageCreateInfo depth_info = {
		.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
		.imageType = VK_IMAGE_TYPE_2D,
		.format = VK_FORMAT_D32_SFLOAT,
		.extent = (VkExtent3D){
			swapchain->extent.width,
			swapchain->extent.height,
			1,
		},
		.mipLevels = 1,
		.arrayLayers = 1,
		.samples = VK_SAMPLE_COUNT_1_BIT,
		.tiling = VK_IMAGE_TILING_OPTIMAL,
		.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
	};

	VulkAllocCreateInfo create_info = {
		.type = VULK_ALLOC_TYPE_IMAGE,
		.info.image = &depth_info,
	};

	if (!vulk_alloc(device, &create_info, &swapchain->depth_image)) {
		perr("Failed to allocate depth image");
		return false;
	}

	VkImageViewCreateInfo depth_view_info = {
		.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
		.viewType = VK_IMAGE_VIEW_TYPE_2D,
		.image = swapchain->depth_image.handle.image,
		.format = VK_FORMAT_D32_SFLOAT,
		.subresourceRange = (VkImageSubresourceRange){
			.baseMipLevel = 0,
			.levelCount = 1,
			.baseArrayLayer = 0,
			.layerCount = 1,
			.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT,
		},
	};
	VkResult ret = device->fn.vkCreateImageView(device->handle,
			&depth_view_info, NULL, &swapchain->depth_view);
	if (VULK_IS_ERR(ret)) {
		perr("Failed to create depth image view (%s)", vulk_strerr(ret));
		return false;
	}

	return true;
}

VulkSwapchain *
vulk_swp_create(Window win, VulkDevice *device)
{
	VkSurfaceCapabilitiesKHR *caps = &device->surface_props.capabilities;
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
			device->physical, win_get_surface(win), caps);

	VulkSwapchain *swapchain = mem_alloc(sizeof(*swapchain));

	/* Prefer triple-buffering but fallback on min or max */
	swapchain->image_count = MAX(caps->minImageCount, 3U);
	if (caps->maxImageCount > 0) {
		swapchain->image_count = MIN(swapchain->image_count,
				caps->maxImageCount);
	}

	swapchain->format = vulk_swp_get_format(&device->surface_props);
	swapchain->extent = adjust_extent(win, &device->surface_props);
	swapchain->mode = choose_present_mode(&device->surface_props);
	VkSwapchainCreateInfoKHR info = {
		.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
		.surface = win_get_surface(win),
		.minImageCount = swapchain->image_count,
		.imageFormat = swapchain->format.format,
		.imageColorSpace = swapchain->format.colorSpace,
		.imageExtent = swapchain->extent,
		.imageArrayLayers = 1,
		.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
		/* This is changed below if there are multiple queues */
		.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE,
		.preTransform = caps->currentTransform,
		.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
		.presentMode = swapchain->mode,
		.clipped = VK_TRUE,
	};

	uint32_t *tmp = device->queue_family_indices;
	uint32_t indices[2];
	if (GET_GFX_FAM_INDEX(tmp) != GET_PRS_FAM_INDEX(tmp)) {
		info.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		info.queueFamilyIndexCount = 2;
		info.pQueueFamilyIndices = indices;
		indices[0] = GET_GFX_FAM_INDEX(tmp);
		indices[1] = GET_PRS_FAM_INDEX(tmp);
	}

	VkResult ret = device->fn.vkCreateSwapchainKHR(device->handle, &info,
			NULL, &swapchain->handle);
	if (VULK_IS_ERR(ret)) {
		perr("Failed to create swapchain (%s)", vulk_strerr(ret));
		return NULL;
	}

	if (!create_depth_image(device, swapchain))
		return NULL;

	device->fn.vkGetSwapchainImagesKHR(device->handle, swapchain->handle,
			&swapchain->image_count, NULL);
	swapchain->images = mem_alloc(swapchain->image_count * sizeof(VkImage));
	swapchain->image_views = mem_alloc(
			swapchain->image_count * sizeof(VkImageView));
	device->fn.vkGetSwapchainImagesKHR(device->handle, swapchain->handle,
			&swapchain->image_count, swapchain->images);
	create_image_views(device, swapchain);

	return swapchain;
}

void
vulk_swp_destroy(VulkDevice *device, VulkSwapchain *swapchain)
{
	for (size_t i = 0; i < swapchain->image_count; i++) {
		device->fn.vkDestroyImageView(device->handle,
				swapchain->image_views[i], NULL);
	}

	mem_free(swapchain->images);
	mem_free(swapchain->image_views);

	device->fn.vkDestroyImageView(device->handle, swapchain->depth_view, NULL);
	vulk_free(device, &swapchain->depth_image);

	device->fn.vkDestroySwapchainKHR(device->handle, swapchain->handle, NULL);
	mem_free(swapchain);
}

VulkSwapchain *
vulk_swp_recreate(Window win, VulkDevice *device, VulkSwapchain *swapchain)
{
	device->fn.vkDeviceWaitIdle(device->handle);
	vulk_swp_destroy(device, swapchain);
	return vulk_swp_create(win, device);
}
