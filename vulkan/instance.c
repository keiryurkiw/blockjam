/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * BlockJam is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BlockJam is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BlockJam.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stddef.h>
#include <stdint.h>
#define VOLK_IMPLEMENTATION
#include <volk.h>

#include "../utils.h"
#include "utils.h"
#include "instance.h"

static const char *const ext_names[] = {
	VK_KHR_SURFACE_EXTENSION_NAME,
#if defined(VK_USE_PLATFORM_WIN32_KHR)
	VK_KHR_WIN32_SURFACE_EXTENSION_NAME,
#elif defined(VK_USE_PLATFORM_WAYLAND_KHR)
	VK_KHR_WAYLAND_SURFACE_EXTENSION_NAME,
#elif defined(VK_USE_PLATFORM_XCB_KHR)
	VK_KHR_XCB_SURFACE_EXTENSION_NAME,
#endif
};
static const size_t ext_count = sizeof(ext_names) / sizeof(ext_names[0]);

VkInstance
vulk_create_inst(void)
{
	VkResult ret = volkInitialize();
	if (ret != VK_SUCCESS) {
		perr("Failed to initialize Vulkan. "
				"Do you have the Vulkan Loader installed?");
		return VK_NULL_HANDLE;
	}

	VkApplicationInfo app_info = {
		.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
		.pApplicationName = "BlockJam",
		.applicationVersion = VK_MAKE_VERSION(0, 0, 0),
		.pEngineName = "BlockJam Engine",
		.engineVersion = VK_MAKE_VERSION(0, 0, 0),
		.apiVersion = VK_API_VERSION_1_3,
	};

	VkInstanceCreateInfo cinfo = {
		.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
		.pApplicationInfo = &app_info,
		.enabledExtensionCount = ext_count,
		.ppEnabledExtensionNames = ext_names,
	};

	VkInstance instance;
	ret = vkCreateInstance(&cinfo, NULL, &instance);
	if (ret != VK_SUCCESS) {
		perr("Failed to create Vulkan instance (%s)", vulk_strerr(ret));
		return VK_NULL_HANDLE;
	}

	volkLoadInstanceOnly(instance);

	return instance;
}
