/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * BlockJam is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BlockJam is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BlockJam.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VULKAN_DEVICE_H
#define VULKAN_DEVICE_H

#include <stdint.h>
#include <volk.h>

#include "../platform/window.h"

#define GET_GFX_FAM_INDEX(_arr) ((_arr)[0])
#define GET_CMP_FAM_INDEX(_arr) ((_arr)[1])
#define GET_XFR_FAM_INDEX(_arr) ((_arr)[2])
#define GET_PRS_FAM_INDEX(_arr) ((_arr)[3])

typedef struct {
	VkSurfaceFormatKHR *formats;
	uint32_t format_count;
	VkPresentModeKHR *modes;
	uint32_t mode_count;
	VkSurfaceCapabilitiesKHR capabilities;
} VulkSurfProps;

typedef struct {
	/*
	 * We're only going to be using the graphics, compute,
	 * transfer, and present queues in that order.
	 * NOTE: These values should never be changed.
	 */
	uint32_t queue_family_indices[4];
	VkPhysicalDevice physical;
	VkPhysicalDeviceProperties properties;
	VkPhysicalDeviceMemoryProperties mem_properties;

	VulkSurfProps surface_props;

	VkDevice handle;
	struct VolkDeviceTable fn;
	VkQueue gfx_queue;
	VkQueue prs_queue;
} VulkDevice;

VulkDevice *vulk_dev_create(VkInstance inst, Window win);
void vulk_dev_destroy(VulkDevice *device);

const uint32_t *vulk_dev_get_family_indices(VulkDevice *device);

#endif /* VULKAN_DEVICE_H */
