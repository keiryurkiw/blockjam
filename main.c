/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * BlockJam is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BlockJam is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BlockJam.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <volk.h>

#include "platform/time.h"
#include "platform/utils.h"
#include "platform/window.h"
#include "vulkan/device.h"
#include "vulkan/instance.h"
#include "vulkan/swapchain.h"
#include "vulkan/utils.h"

TimeSpec delta_time = { 0 };

static VkInstance vk_instance = VK_NULL_HANDLE;
static Window win = NULL;
static VulkDevice *device = NULL;
static VulkSwapchain *swapchain = NULL;

static bool
init(void)
{
	if (!pf_init())
		return false;

	vk_instance = vulk_create_inst();
	if (!vk_instance)
		goto clean_platform;

	if (!win_init())
		goto clean_instance;

	win = win_create(vk_instance, 1280, 720);
	if (!win)
		goto clean_window_system;

	device = vulk_dev_create(vk_instance, win);
	if (!device)
		goto clean_window;

	swapchain = vulk_swp_create(win, device);
	if (!swapchain)
		goto clean_device;

	return true;

clean_device:
	vulk_dev_destroy(device);
clean_window:
	win_destroy(win, vk_instance);
clean_window_system:
	win_deinit();
clean_instance:
	vkDestroyInstance(vk_instance, NULL);
clean_platform:
	pf_deinit();

	return false;
}

static void
set_window_title_fps(Window win)
{
	char title[64];
	time_t dt = ts_to_time(&delta_time);
	sprintf(title, "BlockJam | FPS: %0.0f", 1.0f / (dt / 1e9f));
	win_set_title(win, title);
}

static void
run(void)
{
	VkSemaphore pres_sem, rend_sem;
	VkSemaphoreCreateInfo sem_info = {
		.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
	};
	device->fn.vkCreateSemaphore(device->handle, &sem_info, NULL, &pres_sem);
	device->fn.vkCreateSemaphore(device->handle, &sem_info, NULL, &rend_sem);

	VkFence rend_fen;
	VkFenceCreateInfo fen_info = {
		.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
	};
	device->fn.vkCreateFence(device->handle, &fen_info, NULL, &rend_fen);

	VkCommandPool command_pool;
	VkCommandPoolCreateInfo cmd_pool_info = {
		.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
		.queueFamilyIndex = GET_GFX_FAM_INDEX(device->queue_family_indices),
		.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
	};
	device->fn.vkCreateCommandPool(device->handle,
			&cmd_pool_info, NULL, &command_pool);

	VkCommandBuffer command_buffer;
	VkCommandBufferAllocateInfo cmd_buf_info = {
		.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
		.commandPool = command_pool,
		.commandBufferCount = 1,
		.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
	};
	device->fn.vkAllocateCommandBuffers(device->handle,
			&cmd_buf_info, &command_buffer);

	uint32_t width, height, old_width, old_height;
	win_get_fb_size(win, &old_width, &old_height);

	VkResult ret;
	VkImageMemoryBarrier2 image_memory_barrier;
	VkDependencyInfo dependency_info;

	while (!win_should_close(win)) {
		TimeSpec ts_start;
		get_time(&ts_start);

		win_poll_events();

		win_get_fb_size(win, &width, &height);
		if (old_width != width || old_height != height) {
			old_width = width;
			old_height = height;

			if (width != 0 && height != 0)
				swapchain = vulk_swp_recreate(win, device, swapchain);
		}

		uint32_t img_idx;
		ret = device->fn.vkAcquireNextImageKHR(device->handle,
				swapchain->handle, 1e9, pres_sem, VK_NULL_HANDLE, &img_idx);
		if (ret == VK_ERROR_OUT_OF_DATE_KHR || ret == VK_SUBOPTIMAL_KHR) {
			if (width != 0 && height != 0)
				swapchain = vulk_swp_recreate(win, device, swapchain);
		} else {
			VULK_ASSERT(ret);
		}

		VkCommandBufferBeginInfo begin_info = {
			.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
			.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
		};
		ret = device->fn.vkBeginCommandBuffer(command_buffer, &begin_info);
		VULK_ASSERT(ret);

		image_memory_barrier = (VkImageMemoryBarrier2){
			.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2,
			.srcStageMask = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
			.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
			.srcAccessMask = VK_ACCESS_NONE,
			.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
			.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
			.newLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
			.image = swapchain->images[img_idx],
			.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
			.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
			.subresourceRange = (VkImageSubresourceRange){
				.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
				.baseMipLevel = 0,
				.levelCount = 1,
				.baseArrayLayer = 0,
				.layerCount = 1,
			},
		};
		dependency_info = (VkDependencyInfo){
			.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO,
			.imageMemoryBarrierCount = 1,
			.pImageMemoryBarriers = &image_memory_barrier,
		};
		device->fn.vkCmdPipelineBarrier2(command_buffer, &dependency_info);

		VkRenderingAttachmentInfo colour_info = {
			.sType = VK_STRUCTURE_TYPE_RENDERING_ATTACHMENT_INFO,
			.imageView = swapchain->image_views[img_idx],
			.imageLayout = VK_IMAGE_LAYOUT_ATTACHMENT_OPTIMAL_KHR,
			.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
			.storeOp = VK_ATTACHMENT_STORE_OP_STORE,
			.clearValue = (VkClearValue){
				.color = { .float32 = { 0.0f, 0.0f, 0.0f, 0.0f } },
			},
		};

		VkRenderingInfo render_info = {
			.sType = VK_STRUCTURE_TYPE_RENDERING_INFO,
			.renderArea = (VkRect2D){ .extent = swapchain->extent },
			.layerCount = 1,
			.colorAttachmentCount = 1,
			.pColorAttachments = &colour_info,
		};

		device->fn.vkCmdBeginRendering(command_buffer, &render_info);
		device->fn.vkCmdEndRendering(command_buffer);

		image_memory_barrier = (VkImageMemoryBarrier2){
			.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2,
			.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
			.dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
			.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
			.dstAccessMask = VK_ACCESS_NONE,
			.oldLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
			.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
			.image = swapchain->images[img_idx],
			.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
			.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
			.subresourceRange = (VkImageSubresourceRange){
				.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
				.baseMipLevel = 0,
				.levelCount = 1,
				.baseArrayLayer = 0,
				.layerCount = 1,
			},
		};
		dependency_info = (VkDependencyInfo){
			.sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO,
			.imageMemoryBarrierCount = 1,
			.pImageMemoryBarriers = &image_memory_barrier,
		};
		device->fn.vkCmdPipelineBarrier2(command_buffer, &dependency_info);

		ret = device->fn.vkEndCommandBuffer(command_buffer);
		VULK_ASSERT(ret);

		VkPipelineStageFlags wait_stage =
			VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		VkSubmitInfo submit_info = {
			.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
			.pWaitDstStageMask = &wait_stage,
			.pWaitSemaphores = &pres_sem,
			.waitSemaphoreCount = 1,
			.pSignalSemaphores = &rend_sem,
			.signalSemaphoreCount = 1,
			.commandBufferCount = 1,
			.pCommandBuffers = &command_buffer,
		};
		ret = device->fn.vkQueueSubmit(device->gfx_queue,
				1, &submit_info, rend_fen);
		VULK_ASSERT(ret);

		VkPresentInfoKHR present_info = {
			.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
			.pSwapchains = &swapchain->handle,
			.swapchainCount = 1,
			.pWaitSemaphores = &rend_sem,
			.waitSemaphoreCount = 1,
			.pImageIndices = &img_idx,
		};
		ret = device->fn.vkQueuePresentKHR(device->gfx_queue, &present_info);
		if (ret == VK_SUBOPTIMAL_KHR || ret == VK_ERROR_OUT_OF_DATE_KHR) {
			if (width != 0 && height != 0)
				swapchain = vulk_swp_recreate(win, device, swapchain);
		} else {
			VULK_ASSERT(ret);
		}

		ret = device->fn.vkWaitForFences(device->handle,
				1, &rend_fen, VK_TRUE, 1e9);
		VULK_ASSERT(ret);
		ret = device->fn.vkResetFences(device->handle, 1, &rend_fen);
		VULK_ASSERT(ret);

		ret = device->fn.vkResetCommandBuffer(command_buffer, 0);
		VULK_ASSERT(ret);

		TimeSpec ts_end;
		get_time(&ts_end);
		ts_sub(&ts_end, &ts_start, &delta_time);
		set_window_title_fps(win);
	}

	ret = device->fn.vkDeviceWaitIdle(device->handle);
	VULK_ASSERT(ret);

	device->fn.vkDestroyFence(device->handle, rend_fen, NULL);
	device->fn.vkDestroySemaphore(device->handle, rend_sem, NULL);
	device->fn.vkDestroySemaphore(device->handle, pres_sem, NULL);
	device->fn.vkDestroyCommandPool(device->handle, command_pool, NULL);
}

static void
deinit(void)
{
	vulk_swp_destroy(device, swapchain);
	vulk_dev_destroy(device);

	win_destroy(vk_instance, win);
	win_deinit();

	vkDestroyInstance(vk_instance, NULL);

	pf_deinit();
}

int
main(void)
{
	if (!init())
		return 1;
	run();
	deinit();
}
