/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * BlockJam is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BlockJam is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BlockJam.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <volk.h>
#include <wayland-client.h>

#include "../utils.h"
#include "utils.h"
#include "window.h"
#include "xdg-shell-client-protocol.h"

/* Cast window from generic type to internal type */
#define WC(_w) ((WlWindow *)_w)

typedef struct {
	struct wl_surface *wl_surface;
	struct xdg_surface *xdg_surface;
	struct xdg_toplevel *xdg_toplevel;
	VkSurfaceKHR vk_surface;

	uint32_t dimensions[2];
	bool should_close;
} WlWindow;

static struct wl_display *wl_display = NULL;
static struct wl_registry *wl_registry = NULL;
static struct wl_compositor *wl_compositor = NULL;
static struct xdg_wm_base *xdg_wm_base = NULL;

static void
xdg_wm_base_ping(void *usr_data, struct xdg_wm_base *xdg_wm_base,
		uint32_t serial)
{
	(void)usr_data;

	xdg_wm_base_pong(xdg_wm_base, serial);
}

static void
reg_add_global(void *usr_data, struct wl_registry *registry,
		uint32_t name, const char *interface, uint32_t version)
{
	(void)usr_data;

	if (strcmp(interface, wl_compositor_interface.name) == 0) {
		wl_compositor = wl_registry_bind(registry, name,
				&wl_compositor_interface, version);
	} else if (strcmp(interface, xdg_wm_base_interface.name) == 0) {
		xdg_wm_base = wl_registry_bind(registry, name,
				&xdg_wm_base_interface, version);

		const struct xdg_wm_base_listener xdg_wm_base_listener = {
			.ping = &xdg_wm_base_ping,
		};
		xdg_wm_base_add_listener(xdg_wm_base, &xdg_wm_base_listener, NULL);
	}
}

bool
win_init(void)
{
	wl_display = wl_display_connect(NULL);
	if (!wl_display) {
		perr("Failed to connect to Wayland");
		return false;
	}

	wl_registry = wl_display_get_registry(wl_display);

	const struct wl_registry_listener listener = {
		.global = &reg_add_global,
	};
	wl_registry_add_listener(wl_registry, &listener, NULL);
	wl_display_roundtrip(wl_display);

	return true;
}

void
win_deinit(void)
{
	xdg_wm_base_destroy(xdg_wm_base);
	wl_compositor_destroy(wl_compositor);
	wl_registry_destroy(wl_registry);
	wl_display_disconnect(wl_display);
}

static void
xdg_surface_configure(void *usr_data, struct xdg_surface *xdg_surface,
		uint32_t serial)
{
	(void)usr_data;

	xdg_surface_ack_configure(xdg_surface, serial);
}

static void
xdg_toplevel_close(void *window, struct xdg_toplevel *toplevel)
{
	(void)toplevel;

	WC(window)->should_close = true;
}

static void
xdg_toplevel_configure(void *window, struct xdg_toplevel *toplevel,
		int32_t width, int32_t height, struct wl_array *states)
{
	(void)states;
	(void)toplevel;

	WC(window)->dimensions[0] = (width == 0) ? 1280 : width;
	WC(window)->dimensions[1] = (height == 0) ? 720 : height;
}

Window
win_create(VkInstance vk_inst, uint32_t width, uint32_t height)
{
	WlWindow *win = mem_zalloc(sizeof(*win));
	win->dimensions[0] = width;
	win->dimensions[1] = height;

	struct wl_surface *wl_surface = wl_compositor_create_surface(wl_compositor);
	win->wl_surface = wl_surface;
	struct xdg_surface *xdg_surface = xdg_wm_base_get_xdg_surface(
			xdg_wm_base, wl_surface);
	win->xdg_surface = xdg_surface;

	const struct xdg_surface_listener xdg_surface_listener = {
		.configure = &xdg_surface_configure,
	};
	xdg_surface_add_listener(xdg_surface, &xdg_surface_listener, NULL);

	struct xdg_toplevel *xdg_toplevel = xdg_surface_get_toplevel(xdg_surface);
	win->xdg_toplevel = xdg_toplevel;

	const struct xdg_toplevel_listener xdg_toplevel_listener = {
		.close = &xdg_toplevel_close,
		.configure = &xdg_toplevel_configure,
	};
	xdg_toplevel_add_listener(xdg_toplevel, &xdg_toplevel_listener, win);

	xdg_toplevel_set_app_id(xdg_toplevel, "blockjam");
	xdg_toplevel_set_title(xdg_toplevel, "BlockJam");

	VkSurfaceKHR vk_surface;
	VkWaylandSurfaceCreateInfoKHR surf_info = {
		.sType = VK_STRUCTURE_TYPE_WAYLAND_SURFACE_CREATE_INFO_KHR,
		.display = wl_display,
		.surface = wl_surface,
	};
	vkCreateWaylandSurfaceKHR(vk_inst, &surf_info, NULL, &vk_surface);
	win->vk_surface = vk_surface;

	return win;
}

void
win_destroy(VkInstance vk_inst, Window win)
{
	(void)vk_inst;

	vkDestroySurfaceKHR(vk_inst, WC(win)->vk_surface, NULL);
	xdg_toplevel_destroy(WC(win)->xdg_toplevel);
	xdg_surface_destroy(WC(win)->xdg_surface);
	wl_surface_destroy(WC(win)->wl_surface);
	mem_free(win);
}

void
win_set_title(Window win, char *title)
{
	xdg_toplevel_set_title(WC(win)->xdg_toplevel, title);
}

void
win_get_fb_size(Window win, uint32_t *restrict width, uint32_t *restrict height)
{
	*width = WC(win)->dimensions[0];
	*height = WC(win)->dimensions[1];
}

void
win_poll_events(void)
{
}

bool
win_should_close(Window win)
{
	return WC(win)->should_close;
}

bool
win_is_present_supported(VkPhysicalDevice device, uint32_t index)
{
	return vkGetPhysicalDeviceWaylandPresentationSupportKHR(
			device, index, wl_display);
}

VkSurfaceKHR
win_get_surface(Window win)
{
	return WC(win)->vk_surface;
}
