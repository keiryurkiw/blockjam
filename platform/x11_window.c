/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * BlockJam is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BlockJam is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BlockJam.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <volk.h>
#include <xcb/xcb.h>

#include "../utils.h"
#include "utils.h"
#include "window.h"

#define ONE_BYTE 8

#define WC(_w) ((XcbWindow *)_w)
#define XCB_EV(_type, _ename, _gename) _type *_ename = (_type *)_gename

typedef struct XcbWindow {
	xcb_window_t id;
	xcb_atom_t wm_delete, wm_protocols;
	VkSurfaceKHR surface;

	bool should_close;

	struct XcbWindow *prev, *next;
} XcbWindow;

static xcb_connection_t *xcb_connection = NULL;
static xcb_screen_t *xcb_screen = NULL;
static XcbWindow *win_reference = NULL;

bool
win_init(void)
{
	xcb_connection = xcb_connect(NULL, NULL);
	if (xcb_connection_has_error(xcb_connection)) {
		perr("Failed to connect to the X server");
		return false;
	}

	xcb_screen = xcb_setup_roots_iterator(xcb_get_setup(xcb_connection)).data;

	return true;
}

void
win_deinit(void)
{
	xcb_disconnect(xcb_connection);
}

static inline xcb_atom_t
get_intern_atom(char *name)
{
	xcb_intern_atom_cookie_t cookie = xcb_intern_atom(
			xcb_connection, false, strlen(name), name);
	xcb_intern_atom_reply_t *reply = xcb_intern_atom_reply(
			xcb_connection, cookie, NULL);

	xcb_atom_t atom = reply->atom;
	free(reply);
	return atom;
}

Window
win_create(VkInstance vk_inst, uint32_t width, uint32_t height)
{
	(void)vk_inst;

	XcbWindow *xcb_win = mem_zalloc(sizeof(*xcb_win));
	xcb_win->id = xcb_generate_id(xcb_connection);

	uint32_t event_values =
		XCB_EVENT_MASK_BUTTON_PRESS | XCB_EVENT_MASK_BUTTON_RELEASE |
		XCB_EVENT_MASK_KEY_PRESS | XCB_EVENT_MASK_KEY_RELEASE |
		XCB_EVENT_MASK_EXPOSURE | XCB_EVENT_MASK_POINTER_MOTION |
		XCB_EVENT_MASK_STRUCTURE_NOTIFY;

	xcb_create_window(
			xcb_connection,
			XCB_COPY_FROM_PARENT, /* Screen depth */
			xcb_win->id,
			xcb_screen->root, /* Parent window */
			0, 0, /* Window location (relative to parent) */
			width, height,
			10, /* Border width */
			XCB_WINDOW_CLASS_INPUT_OUTPUT,
			xcb_screen->root_visual,
			XCB_CW_EVENT_MASK, &event_values);
	win_set_title(xcb_win, "BlockJam");

	xcb_win->wm_delete = get_intern_atom("WM_DELETE_WINDOW");
	xcb_win->wm_protocols = get_intern_atom("WM_PROTOCOLS");
	xcb_change_property(
			xcb_connection,
			XCB_PROP_MODE_REPLACE,
			xcb_win->id,
			xcb_win->wm_protocols,
			XCB_ATOM_ATOM,
			4 * ONE_BYTE,
			1, &xcb_win->wm_delete);

	xcb_map_window(xcb_connection, xcb_win->id);
	xcb_flush(xcb_connection);

	VkXcbSurfaceCreateInfoKHR surf_info = {
		.sType = VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR,
		.connection = xcb_connection,
		.window = xcb_win->id,
	};
	vkCreateXcbSurfaceKHR(vk_inst, &surf_info, NULL, &xcb_win->surface);

	/* We're only going to support one window for now */
	win_reference = xcb_win;

	return xcb_win;
}

void
win_destroy(VkInstance vk_inst, Window win)
{
	vkDestroySurfaceKHR(vk_inst, WC(win)->surface, NULL);
	xcb_destroy_window(xcb_connection, WC(win)->id);
	win_reference = NULL;
	mem_free(win);
	xcb_flush(xcb_connection);
}

void
win_set_title(Window win, char *title)
{
	xcb_change_property(
			xcb_connection,
			XCB_PROP_MODE_REPLACE,
			WC(win)->id,
			XCB_ATOM_WM_NAME,
			XCB_ATOM_STRING,
			ONE_BYTE,
			strlen(title), title);
	xcb_flush(xcb_connection);
}

void
win_get_fb_size(Window win, uint32_t *restrict width, uint32_t *restrict height)
{
	xcb_get_geometry_cookie_t cookie = xcb_get_geometry(
			xcb_connection, WC(win)->id);
	xcb_get_geometry_reply_t *reply = xcb_get_geometry_reply(
			xcb_connection, cookie, NULL);

	*width = reply->width;
	*height = reply->height;

	free(reply);
}

void
win_poll_events(void)
{
	xcb_generic_event_t *event;
	XcbWindow *xcb_win = win_reference;

	while ((event = xcb_poll_for_event(xcb_connection)) != NULL) {
		switch (event->response_type & ~0x80) {
			case XCB_CLIENT_MESSAGE: {
				XCB_EV(xcb_client_message_event_t, cme, event);
				if (cme->data.data32[0] == xcb_win->wm_delete)
					xcb_win->should_close = true;
			} break;
		}

		free(event);
	}
}

bool
win_should_close(Window win)
{
	return WC(win)->should_close;
}

bool
win_is_present_supported(VkPhysicalDevice device, uint32_t index)
{
	return vkGetPhysicalDeviceXcbPresentationSupportKHR(
			device, index, xcb_connection, xcb_screen->root_visual);
}

VkSurfaceKHR
win_get_surface(Window win)
{
	return WC(win)->surface;
}
