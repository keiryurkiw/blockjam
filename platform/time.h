/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * BlockJam is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BlockJam is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BlockJam.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PLATFORM_TIME_H
#define PLATFORM_TIME_H

#include <time.h>

#ifdef OS_win32
typedef struct {
	time_t tv_sec;
	long tv_nsec;
} TimeSpec;
#else
typedef struct timespec TimeSpec;
#endif /* OS_win32 */

void get_time(TimeSpec *ts);

inline void
ts_sub(TimeSpec *restrict a, TimeSpec *restrict b, TimeSpec *restrict out)
{
	out->tv_sec = a->tv_sec - b->tv_sec;
	out->tv_nsec = a->tv_nsec - b->tv_nsec;

	if (out->tv_nsec < 0) {
		--out->tv_sec;
		out->tv_nsec = (long)1e9;
	}
}

inline TimeSpec
time_to_ts(time_t time)
{
	return (TimeSpec){
		.tv_sec = time / (time_t)1e9,
		.tv_nsec = time % (long)1e9,
	};
}

inline time_t
ts_to_time(TimeSpec *ts)
{
	return ts->tv_sec*(time_t)1e9 + ts->tv_nsec;
}

#endif /* PLATFORM_TIME_H */
