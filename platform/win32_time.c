/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * This file is part of BlockJam.
 *
 * BlockJam is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BlockJam is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BlockJam.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <assert.h>
#define WIN32_LEAN_AND_MEAN
#define NOGDICAPMASKS
#define NOVIRTUALKEYCODES
#define NOWINMESSAGES
#define NOWINSTYLES
#define NOSYSMETRICS
#define NOMENUS
#define NOICONS
#define NOKEYSTATES
#define NOSYSCOMMANDS
#define NORASTEROPS
#define NOSHOWWINDOW
#define OEMRESOURCE
#define NOATOM
#define NOCLIPBOARD
#define NOCOLOR
#define NOCTLMGR
#define NODRAWTEXT
#define NOGDI
#define NOKERNEL
#define NONLS
#define NOMB
#define NOMEMMGR
#define NOMETAFILE
#define NOMINMAX
#define NOOPENFILE
#define NOSCROLL
#define NOSERVICE
#define NOSOUND
#define NOTEXTMETRIC
#define NOWH
#define NOWINOFFSETS
#define NOCOMM
#define NOKANJI
#define NOHELP
#define NOPROFILER
#define NODEFERWINDOWPOS
#define NOMCX
#include <windows.h>
#include <stdio.h>

#include "time.h"

extern inline void ts_sub(TimeSpec *restrict a,
		TimeSpec *restrict b, TimeSpec *restrict out);
extern inline TimeSpec time_to_ts(time_t time);
extern inline time_t ts_to_time(TimeSpec *ts);

extern LARGE_INTEGER clock_frequency;

void
get_time(TimeSpec *ts)
{
	assert(clock_frequency.QuadPart != 0 &&
			"get_time() has been called before pf_init()");

	LARGE_INTEGER now;
	QueryPerformanceCounter(&now);
	ts->tv_sec = now.QuadPart / clock_frequency.QuadPart;
	ts->tv_nsec = now.QuadPart % clock_frequency.QuadPart;
	ts->tv_nsec *= 1000000000L / clock_frequency.QuadPart;
}
