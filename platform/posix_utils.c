/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * BlockJam is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BlockJam is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BlockJam.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <errno.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"
#include "../utils.h"

/* Unused here */
bool pf_init(void) { return true; }
void pf_deinit(void) {}

void *
mem_alloc(const size_t size)
{
	assert(size > 0 && "Tried allocating 0 bytes");

	void *ptr = malloc(size);
	if (!ptr) {
		perr("Failed to allocate %zu bytes: %s", size, strerror(errno));
		abort();
	}

	return ptr;
}

void *
mem_zalloc(const size_t size)
{
	/*
	 * This assert() is technically redundant because mem_alloc() already
	 * makes this check, but having it here will make bugs easier to find.
	 */
	assert(size > 0 && "Tried allocating 0 bytes");

	void *ptr = mem_alloc(size);
	memset(ptr, 0x00, size);
	return ptr;
}

void *
mem_realloc(void *ptr, const size_t size)
{
	assert(size > 0 && "Tried allocating 0 bytes");

	ptr = realloc(ptr, size);
	if (!ptr) {
		perr("Failed to reallocate %zu bytes: %s", size, strerror(errno));
		abort();
	}

	return ptr;
}

void
mem_free(void *ptr)
{
	free(ptr);
}
