/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * BlockJam is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BlockJam is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BlockJam.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WINDOW_H
#define WINDOW_H

#include <stdbool.h>
#include <stdint.h>
#include <volk.h>

typedef void * Window;

bool win_init(void);
void win_deinit(void);

Window win_create(VkInstance vk_inst, uint32_t width, uint32_t height);
void win_destroy(VkInstance vk_inst, Window win);

void win_set_title(Window win, char *title);

void win_get_fb_size(Window win,
		uint32_t *restrict width, uint32_t *restrict height);

void win_poll_events(void);
bool win_should_close(Window win);

bool win_is_present_supported(VkPhysicalDevice device, uint32_t index);
VkSurfaceKHR win_get_surface(Window win);

#endif /* WINDOW_H */
