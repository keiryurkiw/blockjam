/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * This file is part of BlockJam.
 *
 * BlockJam is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BlockJam is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BlockJam.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#define WIN32_LEAN_AND_MEAN
#define NOGDICAPMASKS
#define NOVIRTUALKEYCODES
#define NOSYSMETRICS
#define NOMENUS
#define NOICONS
#define NOKEYSTATES
#define NORASTEROPS
#define OEMRESOURCE
#define NOATOM
#define NOCLIPBOARD
#define NOCOLOR
#define NOCTLMGR
#define NODRAWTEXT
#define NOGDI
#define NOKERNEL
#define NONLS
#define NOMB
#define NOMEMMGR
#define NOMETAFILE
#define NOMINMAX
#define NOOPENFILE
#define NOSCROLL
#define NOSERVICE
#define NOSOUND
#define NOTEXTMETRIC
#define NOWH
#define NOWINOFFSETS
#define NOCOMM
#define NOKANJI
#define NOHELP
#define NOPROFILER
#define NODEFERWINDOWPOS
#define NOMCX
#include <volk.h>
#include <windows.h>

#include "../utils.h"
#include "utils.h"
#include "window.h"

#define WC(_w) ((Win32Window *)_w)

typedef union {
	ATOM atom;
	LPCTSTR lpctstr;
} WindowClass;

typedef struct Win32Window {
	HWND handle;
	WindowClass class;
	VkSurfaceKHR surface;

	bool should_close;
} Win32Window;

static Win32Window *win_reference = NULL;

extern HINSTANCE hinstance;

static LRESULT CALLBACK process_message(HWND handle, UINT msg,
		WPARAM wparam, LPARAM lparam);

/* Unused here */
bool win_init(void) { return true; }
void win_deinit(void) { }

void *
win_create(VkInstance vk_inst, uint32_t width, uint32_t height)
{
	assert(hinstance != INVALID_HANDLE_VALUE &&
			"Did not call pf_init() before win_create()");

	WindowClass class = { 0 };

	char class_name[32];
	static unsigned int windows_created = 0;
	sprintf(class_name, "blockjam_window_class_%u", windows_created++);

	WNDCLASSEXA wc = {
		.cbSize = sizeof(WNDCLASSEXA),
		.style = CS_DBLCLKS, /* Get double-clicks */
		.lpfnWndProc = process_message,
		.hInstance = hinstance,
		.hCursor = LoadImageA(
				NULL,
				MAKEINTRESOURCEA(IDC_ARROW),
				IMAGE_CURSOR,
				LR_DEFAULTSIZE, LR_DEFAULTSIZE,
				LR_SHARED),
		.lpszClassName = class_name,
	};
	if (!(class.atom = RegisterClassExA(&wc))) {
		win32_perr("Failed to register window");
		return NULL;
	}

	DWORD ex_style = WS_EX_APPWINDOW;
	DWORD style =
		WS_CAPTION | WS_OVERLAPPED | WS_SYSMENU |
		WS_SIZEBOX | WS_MAXIMIZEBOX | WS_MINIMIZEBOX;

	RECT border = { 0 };
	AdjustWindowRectEx(&border, style, FALSE, ex_style);
	width += border.right - border.left;
	height += border.bottom - border.top;

	HANDLE handle = CreateWindowExA(
			ex_style,
			class.lpctstr,
			"BlockJam",
			style,
			CW_USEDEFAULT, CW_USEDEFAULT, /* Window position */
			width, height,
			NULL, NULL, /* Parent window and menu respectively */
			hinstance, NULL);
	if (!handle) {
		win32_perr("Failed to create window");
		UnregisterClassA(class.lpctstr, hinstance);
		return NULL;
	}

	ShowWindow(handle, SW_SHOW);

	VkWin32SurfaceCreateInfoKHR info = {
		.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR,
		.hinstance = hinstance,
		.hwnd = handle,
	};
	VkSurfaceKHR surface;
	vkCreateWin32SurfaceKHR(vk_inst, &info, NULL, &surface);

	Win32Window *win32_win = mem_zalloc(sizeof(*win32_win));
	win32_win->handle = handle;
	win32_win->class = class;
	win32_win->surface = surface;
	win_reference = win32_win;

	return win32_win;
}

void
win_destroy(VkInstance vk_inst, void *win)
{
	vkDestroySurfaceKHR(vk_inst, WC(win)->surface, NULL);

	DestroyWindow(WC(win)->handle);
	BOOL ret = UnregisterClassA(WC(win)->class.lpctstr, hinstance);
	assert(ret && "Window class atom is invalid");

	win_reference = NULL;
	mem_free(win);
}

void
win_set_title(void *win, char *title)
{
	SetWindowTextA(WC(win)->handle, title);
}

void
win_get_fb_size(Window win, uint32_t *restrict width, uint32_t *restrict height)
{
	RECT area;
	GetClientRect(WC(win)->handle, &area);
	*width = area.right;
	*height = area.bottom;
}

static LRESULT CALLBACK
process_message(HWND handle, UINT msg, WPARAM wparam, LPARAM lparam)
{
	switch (msg) {
		case WM_SYSCOMMAND:
			/* Ignore menu events because there isn't a menu */
			if (wparam == SC_KEYMENU)
				return 0;
			break;
		case WM_ERASEBKGND:
			return 1;
		case WM_CLOSE:
			WC(win_reference)->should_close = true;
			return 0;
	}

	return DefWindowProcA(handle, msg, wparam, lparam);
}

void
win_poll_events(void)
{
	MSG msg;
	while (PeekMessageA(&msg, NULL, 0, 0, PM_REMOVE)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}

bool
win_should_close(void *win)
{
	return WC(win)->should_close;
}

bool
win_is_present_supported(VkPhysicalDevice device, uint32_t index)
{
	return vkGetPhysicalDeviceWin32PresentationSupportKHR(device, index);
}

VkSurfaceKHR
win_get_surface(void *win)
{
	return WC(win)->surface;
}
