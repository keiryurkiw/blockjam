/*
 * Copyright (C) 2023  Keir Yurkiw
 *
 * This file is part of BlockJam.
 *
 * BlockJam is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BlockJam is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BlockJam.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#define WIN32_LEAN_AND_MEAN
#define NOGDICAPMASKS
#define NOVIRTUALKEYCODES
#define NOWINMESSAGES
#define NOWINSTYLES
#define NOSYSMETRICS
#define NOMENUS
#define NOICONS
#define NOKEYSTATES
#define NOSYSCOMMANDS
#define NORASTEROPS
#define NOSHOWWINDOW
#define OEMRESOURCE
#define NOATOM
#define NOCLIPBOARD
#define NOCOLOR
#define NOCTLMGR
#define NODRAWTEXT
#define NOGDI
#define NOKERNEL
#define NONLS
#define NOMB
#define NOMEMMGR
#define NOMETAFILE
#define NOMINMAX
#define NOOPENFILE
#define NOSCROLL
#define NOSERVICE
#define NOSOUND
#define NOTEXTMETRIC
#define NOWH
#define NOWINOFFSETS
#define NOCOMM
#define NOKANJI
#define NOHELP
#define NOPROFILER
#define NODEFERWINDOWPOS
#define NOMCX
#include <windows.h>
#ifdef ASAN_ENABLED
#include <string.h> /* For memset() */
#endif /* ASAN_ENABLED */

#include "../utils.h"
#include "utils.h"

HINSTANCE hinstance = INVALID_HANDLE_VALUE;
LARGE_INTEGER clock_frequency = { 0 };

static HANDLE memory_heap = INVALID_HANDLE_VALUE;

void
win32_perr(char *fmt, ...)
{
	fputs("blockjam: \x1b[01;31merror: \x1b[0m", stderr);

	va_list args;
	va_start(args, fmt);
	vfprintf(stderr, fmt, args);
	va_end(args);

	DWORD err_code = GetLastError();
	if (err_code == ERROR_SUCCESS) {
		fputc('\n', stderr);
		return;
	}

	LPTSTR buf;
	DWORD ret = FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL, err_code,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPTSTR)&buf, 0, NULL);
	if (ret > 0) {
		/* Windows adds a newline to the end of buf. */
		fprintf(stderr, ": %s", buf);
		LocalFree(buf);
	} else {
		fputc('\n', stderr);
	}
}

bool
pf_init(void)
{
	hinstance = GetModuleHandleA(NULL);

	QueryPerformanceFrequency(&clock_frequency);

	/* Disable custom memory heaps so that asan can track heap memory. */
#ifndef ASAN_ENABLED
	win32_perr("Hello, world!");
	/* NOTE: Removing serialization also removes thread safety.  This MUST
	 * be removed for multithreading. */
	memory_heap = HeapCreate(HEAP_NO_SERIALIZE, 0, 0);
	if (!memory_heap) {
		win32_perr("Failed to create memory heap");
		return false;
	}
#endif /* ASAN_ENABLED */

	return true;
}

void
pf_deinit(void)
{
#ifndef ASAN_ENABLED
	HeapDestroy(memory_heap);
#endif /* ASAN_ENABLED */
}

static inline void *
internal_alloc(SIZE_T size, DWORD flags)
{
	assert(memory_heap && "mem_alloc() has been called before pf_init()");
	assert(size != 0 && "Tried allocating too little memory");

#ifndef ASAN_ENABLED
	void *ptr = HeapAlloc(memory_heap, flags, size);
#else
	void *ptr = malloc(size);
	if (flags & HEAP_ZERO_MEMORY)
		memset(ptr, 0x00, size);
#endif /* ASAN_ENABLED */
	if (!ptr) {
		perr("Failed to allocate %zu bytes", size);
		abort();
	}

	return ptr;
}

void *
mem_alloc(const size_t size)
{
	return internal_alloc(size, 0);
}

void *
mem_zalloc(const size_t size)
{
	return internal_alloc(size, HEAP_ZERO_MEMORY);
}

void *
mem_realloc(void *ptr, const size_t size)
{
	assert(memory_heap && "mem_realloc() has been called before pf_init()");
	assert(size != 0 && "Tried reallocating too little memory");

#ifndef ASAN_ENABLED
	if (!ptr)
		return mem_alloc(size);

	void *new_ptr = HeapReAlloc(memory_heap, 0, ptr, size);
#else
	void *new_ptr = realloc(ptr, size);
#endif /* ASAN_ENABLED */
	if (!new_ptr) {
		perr("Failed to allocate %zu bytes", size);
		abort();
	}

	return new_ptr;
}

void
mem_free(void *ptr)
{
	assert(memory_heap && "mem_free() has been called before pf_init()");

#ifndef ASAN_ENABLED
	HeapFree(memory_heap, 0, ptr);
#else
	free(ptr);
#endif /* ASAN_ENABLED */
}
